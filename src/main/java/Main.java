import java.time.LocalDate;
import java.time.Period;
import java.util.*;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Provide return day: ");
        LocalDate returnDay = LibraryFine.provideDate();
        System.out.println("Return date: " + returnDay);
        LocalDate expectedReturnDay = LibraryFine.provideDate();
        System.out.println("Expected return date: " + expectedReturnDay);
        int fine = LibraryFine.getFine(returnDay,expectedReturnDay);
        System.out.println("Fine: " + fine);

    }
}
