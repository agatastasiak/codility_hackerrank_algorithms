public class NodesInLinkedList {

    Node head;

    static class Node{
        int data;
        Node next;

        public Node(int d) {
            this.data = d;
            next = null;
        }
    }

    public static NodesInLinkedList insert(NodesInLinkedList list, int data){
        Node newNode = new Node(data);
        newNode.next = null;

        if(list.head == null){
            list.head = newNode;
        }
        else{
            Node last = list.head;
            while (last.next != null){
                last = last.next;
            }
            last.next = newNode;
        }
        return list;
    }

    public static void printList(NodesInLinkedList list) {
        Node currentNode = list.head;
        System.out.println("linkedList: ");
        while(currentNode != null){
            System.out.println(currentNode.data + " ");
            currentNode = currentNode.next;
        }
    }

    public static Node removeDuplicated(Node head){
        Node duplicated;
        if(head == null)
            return null;
        if(head.next != null){
            if(head.data == head.next.data)
            {
                duplicated = head.next;
                head.next = head.next.next;
                removeDuplicated(head);
            }
            else
            {
                removeDuplicated(head.next);
            }
        }
        return head;
    }

    
}
