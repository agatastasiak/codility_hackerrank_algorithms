public class Node {

    int data;
    Node right;
    Node left;
    Node next = null;

//    public Node(int data) {
//        this.data = data;
//        this.left = this.right = null;
//    }

    public Node(int data) {
        this.data = data;
        this.next = null;
    }
}
