import java.util.Scanner;

public class PrimeNotPrime {
    public static int[] provideNumbers() {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = s.nextInt();
        }
        return array;
    }

    public static void primeOrNorPrime(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            PrimeNotPrime.print(primeOrNorPrime(numbers[i]));
        }
    }

    public static boolean primeOrNorPrime(int number) {
        if (number == 1)
            return false;

        if (isPrimeByDefinition(number))
            return true;

        if (isEven(number))
            return false;

        if (hasNoDivisors(number))
            return true;

        return false;

    }

    public static boolean isPrimeByDefinition(int number) {
        if (number == 2)
            return true;
        else
            return false;
    }

    public static boolean isEven(int i) {
        return i % 2 == 0;
    }

    public static boolean hasNoDivisors(int number) {
        for (int j = 2; j <= Math.sqrt(number); j++) {
            if (number % j == 0) {
                return false;
            }
        }
        return true;
    }

    public static void print(boolean isPrime) {
        if (isPrime) {
            System.out.println("Prime");
        } else {
            System.out.println("Not prime");
        }
    }
}
