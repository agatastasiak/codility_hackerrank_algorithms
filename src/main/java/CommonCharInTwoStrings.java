import java.util.ArrayList;
import java.util.List;

// Two strings in two lists, find common substring of each element a.get(i) & b.get(i) and print YES or NO.
public class CommonCharInTwoStrings {
    public static void commonSubstringVerification (List<String> array1, List<String> array2){
                for(int i = 0; i < array1.size(); i++) {
                    if(findCommonChar(array1.get(i),array2.get(i))){
                        printYes();
                    }
                    else{
                        printNo();
                    }
                }
    }

    private static void printYes(){
            System.out.println("YES");
    }

    private static void printNo(){
            System.out.println("NO");
    }

    private static void helperPrint(String array, int index){
        System.out.println(" Value: \"" + array  + "\" Index:" + index +  " Char: " + array.charAt(index) + "\n");
    }

    private static boolean findCommonChar(String array1, String array2){
        for(int k = 0; k < array1.length(); k++){
            for(int l = 0; l < array2.length(); l++){
                helperPrint(array1,k);
                helperPrint(array2,l);
                if((array1.charAt(k) - array2.charAt(l)) == 0){
                    return true;
                }
            }
        }
        return false;
    }
}
