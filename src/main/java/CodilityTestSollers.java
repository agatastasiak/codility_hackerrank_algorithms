import java.util.ArrayList;
import java.util.List;

// create a new number by adding '5' to the existing one in chosen place  and compare which new one is the highest one
public class CodilityTestSollers {

    public static int solution(int N) {
        boolean negative = false;
        if(N<0){
            negative = true;
            N = Math.abs(N);
        }
        String number = String.valueOf(N);
        List<Integer> array = new ArrayList<>();
        for(int i = 0; i < number.length(); i++) {
            array.add(Integer.parseInt(String.valueOf(number.charAt(i))));
        }

        List<Integer> allCombinations = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i <= array.size(); i++){
            sb = new StringBuilder();
            for(int j = 0; j <  array.size(); j++){
                if(i == j ){
                    sb.append(5);
                    sb.append(array.get(j));
                }
                else {
                    sb.append(array.get(j));
                }
                if(i == array.size() && j == array.size()-1){
                    sb.append(5);
                }
            }
            if(negative){
                allCombinations.add(Integer.parseInt(sb.toString())*-1);
            } else {
                allCombinations.add(Integer.parseInt(sb.toString()));
            }
        }
        int max = 0;
        for(int i = 0; i < allCombinations.size(); i++){
            if(max < allCombinations.get(i)){
                max = allCombinations.get(i);
            }
        }
        System.out.println("max: " + max);
        for(int i = 0; i < allCombinations.size(); i++){
            System.out.println(allCombinations.get(i));
        }
        return max;
    }
}
