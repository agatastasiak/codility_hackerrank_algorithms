import java.util.LinkedList;
import java.util.Queue;

public class LevelOrderBST {
    public static void printLevelOrder(Node root){
        if(root == null){
            return;
        }
        else{
            Queue q = new LinkedList();
            q.add(root);
            while(!q.isEmpty()){
                Node temp = (Node) q.remove();
                if(temp.left != null){
                    q.add(temp.left);
                }
                if(temp.right != null){
                    q.add(temp.right);
                }
                System.out.print(temp.data + " ");
            }
        }
    }
}
