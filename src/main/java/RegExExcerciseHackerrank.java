import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class RegExExcerciseHackerrank {


    public static void printList(List<String> addresses) {
        addresses.sort(Comparator.naturalOrder());
        for (String address : addresses) {
            String[] nameAndEmail = address.split("\\s");
            if (ifStringValidated(nameAndEmail[0], "[a-z]{1,20}")
                    && ifStringValidated(nameAndEmail[1], "[a-z]{1,20}"))
                System.out.println(nameAndEmail[0]);
        }
    }

    public static boolean ifStringValidated(String string, String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        return m.matches();
    }

//    public static boolean nameValidator(String name){
//        Pattern p = Pattern.compile("[a-z]{1,20}");
//        Matcher m = p.matcher(name);
//        return m.matches();
//    }
//
//    public static boolean emailValidator(String email){
//        Pattern p = Pattern.compile("^[a-z]{1,40}+@gmail.com$");
//        Matcher m = p.matcher(email);
//        return m.matches();
//    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        List<String> personWithEmail = new ArrayList<>();
        ;

        for (int NItr = 0; NItr < N; NItr++) {

            String[] firstNameEmailID = scanner.nextLine().split(" ");

            String firstName = firstNameEmailID[0];

            String emailID = firstNameEmailID[1];

            personWithEmail.add(firstName + " " + emailID);
        }

        scanner.close();
        printList(personWithEmail);

    }
}
