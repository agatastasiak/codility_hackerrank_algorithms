import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Bitwise {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int t = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int tItr = 0; tItr < t; tItr++) {
            String[] nk = scanner.nextLine().split(" ");

            int n = Integer.parseInt(nk[0]);

            int k = Integer.parseInt(nk[1]);

            int[] array = new int[n];
            array[0] = 1;
            array[1] = 2;
            array[2] = 3;
            array[3] = 4;
            array[4] = 5;
            int max = findMaxInt(array, k);
            print(max);

        }

        scanner.close();
    }

    public static void upCase() {
        char ch;
        int val = 65503;

        System.out.println("65503 binarnie: " + Integer.toBinaryString(val));

        for (int i = 0; i < 10; i++) {
            int charVal = ((int) 'a' + i);
            System.out.print(charVal + " ");
            System.out.print(Integer.toBinaryString(charVal) + " ");
            ch = (char) charVal;
            System.out.print(ch);

            ch = (char) ((int) ch & 65503);
            System.out.print(ch + " ");
        }
    }

    public static void showBits() {
        int t;
        byte val;

        val = 123;
        System.out.println("123 binarnie: " + Integer.toBinaryString(val));
        for (t = 128; t > 0; t = t / 2) {
            System.out.println("t: " + t + " " + Integer.toBinaryString(t));
            if ((val & t) != 0)
                System.out.print("1 ");
            else
                System.out.print("0 ");

        }
    }

    public static int findMaxInt(int[] array, int k) {
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                int temp = (array[i] & array[j]);
                if ( temp > max && temp < k)
                    max = temp;
            }
        }
        return max;
    }

    public static void print(int n) {
        System.out.println(n);
    }
}
