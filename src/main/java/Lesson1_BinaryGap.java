

import java.util.ArrayList;
import java.util.Scanner;

public class Lesson1_BinaryGap {

    public static void printDecimalToBinary() {
        int n = giveANumber();
        if (n > 1) {
            ArrayList<Integer> binary = convertToBinary(n);
            System.out.print("Binary representation of " + n + ": ");
            for (Integer i : binary) {
                System.out.print(i);
            }
        } else {
            System.out.println("Wrong number, it should have been positive");
        }
    }

    private static int giveANumber() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please provide a positive number");
        int n = s.nextInt();
        return n;
    }

    private static ArrayList<Integer> convertToBinary(int n) {
        ArrayList<Integer> restOfDivision = new ArrayList<Integer>();
        ArrayList<Integer> binary = new ArrayList<Integer>();
        for (int i = 0; n >= 1; i++) {
            int r = n % 2;
            n = n / 2;
            restOfDivision.add(r);
        }
        for (int i = restOfDivision.size() - 1; i >= 0; i--) {
            int b = restOfDivision.get(i);
            binary.add(b);
        }
        return binary;
    }

    private static String convertDecimalToStringUsingJavaMethod(int n) {
        return Integer.toBinaryString(n);
    }

    private static String[] convertArrayIntToString(ArrayList<Integer> binaryInt) {
        String s[] = new String[binaryInt.size()];
        for (int i = 0; i < binaryInt.size(); i++) {
            s[i] = String.valueOf(binaryInt.get(i));
        }
        return s;
    }

    public static boolean compateTwoBinaryResults(int n) {
        String binaryString = convertDecimalToStringUsingJavaMethod(n);
        ArrayList<Integer> binaryInteger = convertToBinary(n);
        String[] binaryString2 = convertArrayIntToString(binaryInteger);
        StringBuilder builder = new StringBuilder();
        for (String s : binaryString2) {
            builder.append(s);
        }
        String str = builder.toString();
        return binaryString.equals(str);
    }

    public static int countBinaryGap(int n) {
        String b = Integer.toBinaryString(n);
        int maxResult = 0;
//                    for(int i = 0; i < binary.length(); i++){
////                        if(binary.contains("1" + "0".repeat(i) + "1")){
////                            maxResult = i;
////                        }
//                    }
        for (int i = 0; i < b.length(); i++) {
            if (b.charAt(i) == '1') {
                int zeroCount = 0;
                for (int j = i + 1; j < b.length(); j++) {
                    if (b.charAt(j) == '0') {
                        zeroCount++;
                    } else {
                        if (zeroCount > maxResult) {
                            maxResult = zeroCount;
                        }
                        i = i + zeroCount;
                        break;
                    }
                }
            }
        }
        return maxResult;
    }
}


