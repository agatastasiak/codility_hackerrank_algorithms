import java.util.Random;

// Calculate the hourglass sum for every hourglass in A, then print the maximum hourglass sum.
public class HourGlass {


    public static  int maxSumOfHourglass(int[][] hourGlass){
        int max_sum = Integer.MIN_VALUE;
        for(int i = 1; i < hourGlass.length-1; i++){
            for(int j = 1; j < hourGlass[i].length-1; j++){
                int sum = hourGlass[i][j]
                        + hourGlass[i-1][j-1]
                        + hourGlass[i-1][j]
                        + hourGlass[i-1][j+1]
                        + hourGlass[i+1][j-1]
                        + hourGlass[i+1][j]
                        + hourGlass[i+1][j+1];
                max_sum = Math.max(max_sum,sum);
            }
        }
        return max_sum;
    }

    public static int[][] create2DArray(int rowSize, int colSize){
        int[][] arr = new int[rowSize][colSize];
        Random r = new Random();
        for(int i = 0; i < rowSize; i++){
            for(int j = 0; j < colSize; j++){
                arr[i][j] = r.nextInt(5+1);
            }
        }
        return arr;
    }
}