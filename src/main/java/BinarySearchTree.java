public class BinarySearchTree {
    public static Node insert(Node root,int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }

    public static int getHeight(Node root){
        if(root == null){
            return -1;
        }
        else{
            int leftHeight = getHeight(root.left);
            int rightHeight = getHeight(root.right);

            if(leftHeight > rightHeight){
               return (leftHeight + 1);
            }
            else{
                return (rightHeight + 1);
            }
        }
    }
}
