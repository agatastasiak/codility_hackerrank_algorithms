import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Scanner;

public class LibraryFine {

    private static Scanner s = new Scanner(System.in).useDelimiter("\\s");

    public static LocalDate provideDate() {
        int day = s.nextInt();
        int month = s.nextInt();
        int year = s.nextInt();
        LocalDate date = LocalDate.of(year, month, day);
        return date;
    }

    public static int getFine(LocalDate returnDate, LocalDate expectedReturnDate) {
        if (Period.between(expectedReturnDate, returnDate).getDays()> 0) {
            int years = returnDate.getYear() - expectedReturnDate.getYear();
            if (years != 0) {
                return 10000;
            }
            int months = returnDate.getMonthValue() - expectedReturnDate.getMonthValue();
            if (months != 0) {
                return months * 500;
            }
            int days = returnDate.getDayOfMonth() - expectedReturnDate.getDayOfMonth();
            if (days != 0) {
                return days * 15;
            }
        }
        return 0;
    }
}
