public class MaximumDifference_Scoper {

    private int[] elements;
    public int maximumDifference;

    public MaximumDifference_Scoper(int[] elements) {
        this.elements = elements;
    }

    public int computeDifference(){
        this.maximumDifference = 0;
        for(int i = 0; i < this.elements.length; i++){
            for(int j = 0; j < this.elements.length; j++){
                int absDiff = Math.abs(elements[i]-elements[j]);
                if(absDiff > this.maximumDifference){
                    maximumDifference = absDiff;
                }
            }
        }
        return this.maximumDifference;
    }
}
