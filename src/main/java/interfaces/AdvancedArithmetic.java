package interfaces;

public interface AdvancedArithmetic {
    // publiczne statyczne pole finalne - można pominąć public static final
    int CONSTANT_VARIABLE = 5;
    int divisorSum(int n);

    // defaultowa publiczna metoda z ciałem, może być nadpisana przez klasy implementujące
    default void testDefaultMethod() {

    }
    //prywatna metoda z ciałem, może być wywołana wyłaćznie w definicji interfejsu
    //  by usunąc powtarzajacy sie kod , uzyc ich w metodach domyslnych
    private int testPrivateIntMethod(){
        return 1;
    }

    // publiczna statyczna metoda z ciałem
    static void privateStaticMethodTest() {

    }
}
