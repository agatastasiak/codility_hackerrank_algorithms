// Print a single base- integer denoting the
// maximum number of consecutive 's in the binary representation of .
public class BinaryNumbers_lesson10Hackerrank {
    public static int consecutiveOnesInBinaryRepresenation(int n) {
        return countConsecutiveOnes(convertIntegerToBinary(n));
    }

    private static String convertIntegerToBinary(int n) {
        return Integer.toBinaryString(n);
    }

    private static int countConsecutiveOnes(String s) {
        int counterLeft = 0;
        for (char ch : s.toCharArray()) {
            if (ch == '1') {
                counterLeft++;
            }
            if(ch == '0'){
                break;
            }
        }
        int counterRight = 0;
        for(int i = s.length()-1; i >=0; i--){
            if(s.charAt(i) == '1'){
                counterRight++;
            }
            if(s.charAt(i) == '0'){
                break;
            }
        }
        if(counterLeft > counterRight){
            return counterLeft;
        }
        else
        return counterRight;
    }
}
