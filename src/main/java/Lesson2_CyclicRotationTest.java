
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Lesson2_CyclicRotationTest {

    @BeforeAll
    public static void setUp(){
        int k = 3;
        int[] arrayPositiv = new int[] {1,2,3,4,5,6};
        int[] arrayPositivAfterRotation = new int[] {5,6,1,2,3,4};
        int[] arrayNegative = new int[] {-1,-2,-3,-4,-5,-6};
        int[] arrayNegativeAfterRotation = new int[] {4,-5,-6,-1,-2,-3};
        int[] arrayWithZeros = new int[] {0,0,0,0,};
    }

    @Test
    void whenRotatedNumberGiven_ThenExpectCorrectKTimesRotation(int[] arrayPositiv, int k, int[] arrayPositivAfterRotation ) {
        int[] actualRotationResult = Lesson2_CyclicRotation.solution(arrayPositiv,k);
        assertEquals(arrayPositiv.length, actualRotationResult.length);
        for(int i = 0; i < actualRotationResult.length; i++){
            assertEquals(arrayPositivAfterRotation[i], actualRotationResult[i]);
        }
    }

    @Test
    void whenRotatedNumberGiven_ThenExpectCorrectKTimesRotationForNegativeElements(int[] arrayNegative, int k, int[] arrayNegativeAfterRotation ) {
        int[] actualRotationResult = Lesson2_CyclicRotation.solution(arrayNegative,k);
        assertEquals(arrayNegative.length, actualRotationResult.length);
        for(int i = 0; i < actualRotationResult.length; i++){
            assertEquals(arrayNegativeAfterRotation[i], actualRotationResult[i]);
        }
    }
     @Test
        void whenRotatedNumberTheSame_ThenExpectNoChanges(int[] arrayWithZeros, int k) {
            int[] actualRotationResult = Lesson2_CyclicRotation.solution(arrayWithZeros,k);
            for(int i = 0; i < arrayWithZeros.length; i++){
                assertEquals(arrayWithZeros[i], actualRotationResult[i]);
            }
        }

}