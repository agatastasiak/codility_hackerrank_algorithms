import java.util.Arrays;

public class BubbleSort {
    // sorting in ascending order
    static int[] array = {5,6,9,0,1,6,3,7};
    public static int[] bubbleSortInAscendingOrder(int[] array){

        int swap = 0;
        for(int i = 0 ; i < array.length; i++){
            for(int j = 0; j < array.length-1; j++){
                if(array[j+1] < array[j]){
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                    swap++;
                }
            }
        }
        System.out.println("Number of swaps: " + swap);
        return array;
    }
    public static void  printArray(int[] array){
        for(int i : array){
            System.out.print(i + " ");
        }
        System.out.println("\n");
    }

    public static void compareToQuickSort(int[] array){
        long startTime = System.nanoTime();
        bubbleSortInAscendingOrder(BubbleSort.array);
        long endTime = System.nanoTime();
        System.out.println("Duration of bubble sort: " + ((double)(endTime-startTime)/1000000));

        long startTimeJavaSort = System.nanoTime();
        Arrays.sort(BubbleSort.array);
        long endTimeJavaSort = System.nanoTime();
        System.out.println("Duration of quick sort: " + ((double)(endTimeJavaSort-startTimeJavaSort)/1000000));
    }
}
