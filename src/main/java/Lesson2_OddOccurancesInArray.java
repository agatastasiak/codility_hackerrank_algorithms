import java.util.Arrays;

public class Lesson2_OddOccurancesInArray {

    public static int solution(int[] A) {
        int unpaired = 0; // the value of the unpaired element at index i
        Arrays.sort(A);
            for (int i = 0; i < A.length; i += 2) {
                if ( (i+1 < A.length) && A[i] != A[i + 1]) {
                    unpaired = i;
                    break;
                } else {
                    continue;
                }
            }
        return A[unpaired];
    }
}
