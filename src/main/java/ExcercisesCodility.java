/*We can keep generating and printing consecutive Fibonacci numbers until we exceed n.
        In each step it’s enough to store only two consecutive Fibonacci numbers.*/

import org.w3c.dom.ls.LSOutput;

import javax.swing.plaf.basic.BasicBorders;
import java.lang.reflect.Array;

public class ExcercisesCodility {

    public static void printStarsUpsideDown(int n){
        int s = 2 * n - 1;
        for(int j = 1; j <= n; j++) {
            System.out.print(" ".repeat(2*j-2));
            for (int z = s ; z >= 1; z--) {
                System.out.print("* ");
            }
            s=s-2;
            System.out.println("");
        }
    }

    public static int[] createFibonacci(int n){
//        int n - the final number of Fibonacci numbers to archive
        int[] fibonacciNumbers = new int[n+1];
        fibonacciNumbers[0] = 0;
        fibonacciNumbers[1] = 1;
        if(n <= 2){
            System.out.println(fibonacciNumbers[0] + " " + fibonacciNumbers[1]);
        }
        else{
            for(int i = 3; i <= n; i++){
                fibonacciNumbers[i] = fibonacciNumbers[i-1] + fibonacciNumbers[i-2];
            }
        }
        return fibonacciNumbers;
    }

    public static void printFibonacci(int[] fibonacciNumbers){
        int i = 0;
        while(i < fibonacciNumbers.length-1){
            System.out.print(fibonacciNumbers[i] + " ");
            i++;
        }
    }

    public static int sumRecursion(int n){
        if(n > 0){
            return n + sumRecursion(n-1);
        }
        else{
            return 0;
        }
    }

    public static int fibonacciRecursion(int n) {
            if(n == 0){
                return 0;
            }
            if(n == 1 || n == 2){
                return 1;
            }
            return fibonacciRecursion(n-2) + fibonacciRecursion(n-1);
    }
}


