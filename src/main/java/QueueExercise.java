import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class QueueExercise {

    // 1
    Stack stackQueue = new Stack();
    Queue listQueue = new LinkedList();

    //2
    void pushCharacter(char ch){
        this.stackQueue.push(ch);
    }
    //3
    void enqueueCharacter(char ch){
        this.listQueue.add(ch);
    }
    //4
    char popCharacter(){
        return (char) this.stackQueue.pop();
    }
    //5
    char dequeueCharacter(){
        return (char) this.listQueue.remove();
    }
}
