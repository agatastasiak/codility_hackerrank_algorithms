public class MyBook extends Book {

    int price;

    public MyBook(String title, String author, int price) {
        super(title, author);
        this.price = price;
    }

    @Override
    public void display() {
        System.out.println(title);
        System.out.println(author);
        System.out.println(price);
    }

    @Override
    public void anotheMethod() {

    }
}
