import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * int k - number of rotation
 * @return - new int[] array rotated k-times
 */
public class Lesson2_CyclicRotation {

    public static int[] solution(int[] array, int k) {
        int[] rotatedArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int rotatedIndex = (i + k) % array.length;
            rotatedArray[rotatedIndex] = array[i];
        }
        return rotatedArray;
    }

    public static void printArrayElements(int array[]){
        for(int i = 0 ; i < array.length; i++){
            System.out.println("i: " + i + " = " + array[i]);
        }
        System.out.println("\n");
    }
}